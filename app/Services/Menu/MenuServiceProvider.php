<?php

namespace App\Services\Menu;

use Illuminate\Support\ServiceProvider;
use Konekt\Menu\Facades\Menu;

class MenuServiceProvider extends ServiceProvider
{

    public function boot(){
        $this->addMenuItems();
    }

    private function addMenuItems()
    {
        if ($menu = Menu::get('left_nav_menu')) {
            $navigation = $menu->addItem('navigation', __('Navigation'));
            $dashboard = $menu->addItem('dashboard', __('Dashboard'))
                ->data('icon', 'airplay')
                ->data('class_text', 'badge badge-success badge-pill float-right')
                ->data('right_text', 4);
            /*$dashboard->addSubItem('dashboard', __('Dashboard'), ['route' => 'vanilo.dashboard.index'])
                ->activateOnUrls(route('vanilo.dashboard.index', [], false) . '*')
                ->allowIfUserCan('list products');*/

            $dashboard->addSubItem('dashboard-1', __('Dashboard'), ['url' => '/dashboard']);
            $dashboard->addSubItem('dashboard-2', __('Dashboard 2'), ['url' => '/dashboard-2']);
            $dashboard->addSubItem('dashboard-3', __('Dashboard 3'), ['url' => '/dashboard-3']);
            $dashboard->addSubItem('dashboard-4', __('Dashboard 4'), ['url' => '/dashboard-4']);

            $apps = $menu->addItem('apps', __('Apps'), ['class' => 'mt-2']);
            $calendar = $menu->addItem('calendar', __('Calendar'),['url'=>'/apps/calendar'])
                ->data('icon', 'calendar');
            $calendar = $menu->addItem('chat', __('Chat'),['url'=>'/apps/chat'])
                ->data('icon', 'message-square');

            $ecommerce = $menu->addItem('ecommerce', __('Ecommerce'))
                ->data('icon', 'shopping-cart')
                ->data('arrow', 'menu-arrow');

            $ecommerce->addSubItem('ecom_dashboard', __('Dashboard'), ['url' => '/ecommerce/dashboard']);
            //$ecommerce->addSubItem('ecom_products', __('Products'), route('lk.products.index'));
            $ecommerce->addSubItem('ecom_products', __('Products'), ['url' => '/#']);
            $ecommerce->addSubItem('ecom_product-detail', __('Product Detail'), ['url' => '/ecommerce/product-detail']);
            $ecommerce->addSubItem('ecom_product-edit', __('Add Product'), ['url' => '/ecommerce/product-edit']);
            $ecommerce->addSubItem('ecom_customers', __('Customers'), ['url' => '/ecommerce/customers']);
            $ecommerce->addSubItem('ecom_orders', __('Orders'), ['url' => '/ecommerce/orders']);
            $ecommerce->addSubItem('ecom_order-detail', __('Order Detail'), ['url' => '/ecommerce/order-detail']);
            $ecommerce->addSubItem('ecom_sellers', __('Sellers'), ['url' => '/ecommerce/sellers']);
            //$ecommerce->addSubItem('ecom_cart', __('Shopping Cart'), route('lk.cart.index'));
            $ecommerce->addSubItem('ecom_cart', __('Shopping Cart'), ['url' => '/#']);
            $ecommerce->addSubItem('ecom_checkout', __('Checkout'), ['url' => '/ecommerce/checkout']);


            $crm = $menu->addItem('crm', __('CRM'))
                ->data('icon', 'users')
                ->data('arrow', 'menu-arrow');
            $crm->addSubItem('crm_dashboard', __('Dashboard'), ['url' => '/crm/dashboard']);
            $crm->addSubItem('crm_contacts', __('Contacts'), ['url' => '/crm/contacts']);
            $crm->addSubItem('crm_opportunities', __('Opportunities'), ['url' => '/crm/opportunities']);
            $crm->addSubItem('crm_leads', __('Leads'), ['url' => '/crm/leads']);
            $crm->addSubItem('crm_customers', __('Customers'), ['url' => '/crm/customers']);


            $email = $menu->addItem('email', __('Email'))
                ->data('icon', 'mail')
                ->data('arrow', 'menu-arrow');
            $email->addSubItem('email_inbox', __('Inbox'), ['url' => '/email/inbox']);
            $email->addSubItem('email_read', __('Read Email'), ['url' => '/email/read']);
            $email->addSubItem('email_compose', __('Compose Email'), ['url' => '/email/compose']);
            $email->addSubItem('email_templates', __('Email Templates'), ['url' => '/email/templates']);


            $rrs = $menu->addItem('rss', __('Social Feed'),['url'=>'/apps/social-feed'])
                ->data('icon', 'rss')
                ->data('class_text', 'badge badge-pink float-right')
                ->data('right_text', 'Hot');

            $rrs = $menu->addItem('companies', __('Companies'),['url'=>'/apps/companies'])
                ->data('icon', 'activity');


            $projects = $menu->addItem('projects', __('Projects'))
                ->data('icon', 'briefcase')
                ->data('arrow', 'menu-arrow');
            $projects->addSubItem('projects_list', __('List'), ['url' => '/project/list']);
            $projects->addSubItem('projects_detail', __('Detail'), ['url' => '/project/detail']);
            $projects->addSubItem('projects_create', __('Create Project'), ['url' => '/project/create']);

            $tasks = $menu->addItem('tasks', __('Tasks'))
                ->data('icon', 'clipboard')
                ->data('arrow', 'menu-arrow');
            $tasks->addSubItem('tasks_list', __('List'), ['url' => '/task/list']);
            $tasks->addSubItem('tasks_details', __('Details'), ['url' => '/task/details']);
            $tasks->addSubItem('tasks_kanban-board', __('Kanban Board'), ['url' => '/task/kanban-board']);

            $contacts = $menu->addItem('contacts', __('Contacts'))
                ->data('icon', 'book')
                ->data('arrow', 'menu-arrow');
            $contacts->addSubItem('contacts_list', __('Members List'), ['url' => '/contacts/list']);
            $contacts->addSubItem('contacts_profile', __('Profile'), ['url' => '/contacts/profile']);

            $tickets = $menu->addItem('tickets', __('Tickets'))
                ->data('icon', 'aperture')
                ->data('arrow', 'menu-arrow');
            $tickets->addSubItem('tickets_list', __('List'), ['url' => '/tickets/list']);
            $tickets->addSubItem('tickets_detail', __('Detail'), ['url' => '/tickets/detail']);


            $rrs = $menu->addItem('file-manager', __('File Manager'),['url'=>'/apps/file-manager'])
                ->data('icon', 'folder-plus');

            $custom = $menu->addItem('custom', __('Custom'),['class'=>'mt-2']);

            $pages = $menu->addItem('auth_pages', __('Auth Pages'))
                ->data('icon', 'file-text')
                ->data('arrow', 'menu-arrow');
            $pages->addSubItem('login', __('Log In'), ['url' => '/auth/login']);
            $pages->addSubItem('login-2', __('Log In 2'), ['url' => '/auth/login-2']);
            $pages->addSubItem('register', __('Register'), ['url' => '/auth/register']);
            $pages->addSubItem('register-2', __('Register 2'), ['url' => '/auth/register-2']);
            $pages->addSubItem('signin-signup', __('Signin - Signup'), ['url' => '/auth/signin-signup']);
            $pages->addSubItem('signin-signup-2', __('Signin - Signup 2'), ['url' => '/auth/signin-signup-2']);
            $pages->addSubItem('recoverpw', __('Recover Password'), ['url' => '/auth/recoverpw']);
            $pages->addSubItem('recoverpw-2', __('Recover Password 2'), ['url' => '/auth/recoverpw-2']);
            $pages->addSubItem('lock-screen', __('Lock Screen'), ['url' => '/auth/lock-screen']);
            $pages->addSubItem('lock-screen-2', __('Lock Screen 2'), ['url' => '/auth/lock-screen-2']);
            $pages->addSubItem('logout', __('Logout'), ['url' => '/auth/logout']);
            $pages->addSubItem('logout-2', __('Logout'), ['url' => '/auth/logout-2']);
            $pages->addSubItem('confirm-mail', __('Confirm Mail'), ['url' => '/auth/confirm-mail']);
            $pages->addSubItem('confirm-mail-2', __('Confirm Mail 2'), ['url' => '/auth/confirm-mail-2']);




            $extra = $menu->addItem('extra', __('Extra Pages'))
                ->data('icon', 'package')
                ->data('arrow', 'menu-arrow');
            $extra->addSubItem('starter', __('Starter'), ['url' => '/pages/starter']);
            $extra->addSubItem('timeline', __('Timeline'), ['url' => '/pages/timeline']);
            $extra->addSubItem('sitemap', __('Sitemap'), ['url' => '/pages/sitemap']);
            $extra->addSubItem('invoice', __('Invoice'), ['url' => '/pages/invoice']);
            $extra->addSubItem('faqs', __('FAQs'), ['url' => '/pages/faqs']);
            $extra->addSubItem('search-results', __('Search Results'), ['url' => '/pages/search-results']);
            $extra->addSubItem('pricing', __('Pricing'), ['url' => '/pages/pricing']);
            $extra->addSubItem('maintenance', __('Maintenance'), ['url' => '/pages/maintenance']);
            $extra->addSubItem('coming-soon', __('Coming Soon'), ['url' => '/pages/coming-soon']);
            $extra->addSubItem('gallery', __('Gallery'), ['url' => '/pages/gallery']);
            $extra->addSubItem('404', __('Error 404'), ['url' => '/pages/404']);
            $extra->addSubItem('404-two', __('Error 404 Two'), ['url' => '/pages/404-two']);
            $extra->addSubItem('404-alt', __('Error 404-alt'), ['url' => '/pages/404-alt']);
            $extra->addSubItem('500', __('Error 500'), ['url' => '/pages/500']);
            $extra->addSubItem('500-two', __('Error 500 Two'), ['url' => '/pages/500-two']);



            $layouts = $menu->addItem('layouts', __('Layouts'))
                ->data('icon', 'layout')
                ->data('right_text', 'New')
                ->data('class_text', 'badge badge-blue float-right');
            $layouts->addSubItem('horizontal', __('Horizontal'), ['url' => '/layoutsDemo/horizontal']);
            $layouts->addSubItem('detached', __('Detached'), ['url' => '/layoutsDemo/detached']);
            $layouts->addSubItem('two-column', __('Two Column Menu'), ['url' => '/layoutsDemo/two-column']);
            $layouts->addSubItem('two-tone-icons', __('Two Tones Icons'), ['url' => '/layoutsDemo/two-tone-icons']);
            $layouts->addSubItem('preloader', __('Preloader'), ['url' => '/layoutsDemo/preloader']);


            $components = $menu->addItem('components', __('Components'),['class'=>'mt-2']);


            $base_ui = $menu->addItem('base_ui', __('Base UI'))
                ->data('icon', 'pocket')
                ->data('arrow', 'menu-arrow');
            $base_ui->addSubItem('buttons', __('Buttons'), ['url' => '/ui/buttons']);
            $base_ui->addSubItem('cards', __('Cards'), ['url' => '/ui/cards']);
            $base_ui->addSubItem('avatars', __('Avatars'), ['url' => '/ui/avatars']);
            $base_ui->addSubItem('portlets', __('Portlets'), ['url' => '/ui/portlets']);
            $base_ui->addSubItem('tabs-accordions', __('Tabs & Accordions'), ['url' => '/ui/tabs-accordions']);
            $base_ui->addSubItem('modals', __('Modals'), ['url' => '/ui/modals']);
            $base_ui->addSubItem('progress', __('Progress'), ['url' => '/ui/progress']);
            $base_ui->addSubItem('notifications', __('Notifications'), ['url' => '/ui/notifications']);
            $base_ui->addSubItem('spinners', __('Spinners'), ['url' => '/ui/spinners']);
            $base_ui->addSubItem('images', __('Images'), ['url' => '/ui/images']);
            $base_ui->addSubItem('carousel', __('Carousel'), ['url' => '/ui/carousel']);
            $base_ui->addSubItem('list-group', __('List Group'), ['url' => '/ui/list-group']);
            $base_ui->addSubItem('video', __('Embed Video'), ['url' => '/ui/video']);
            $base_ui->addSubItem('dropdowns', __('Dropdowns'), ['url' => '/ui/dropdowns']);
            $base_ui->addSubItem('ribbons', __('Ribbons'), ['url' => '/ui/ribbons']);
            $base_ui->addSubItem('tooltips-popovers', __('Tooltips & Popovers'), ['url' => '/ui/tooltips-popovers']);
            $base_ui->addSubItem('general', __('General UI'), ['url' => '/ui/general']);
            $base_ui->addSubItem('typography', __('Typography'), ['url' => '/ui/typography']);
            $base_ui->addSubItem('grid', __('Grid'), ['url' => '/ui/grid']);


            $extended_ui = $menu->addItem('extended_ui', __('Extended UI'))
                ->data('icon', 'layers')
                ->data('right_text', 'Hot')
                ->data('class_text', 'badge badge-info float-right');
            $extended_ui->addSubItem('nestable', __('Nestable List'), ['url' => '/extended/nestable']);
            $extended_ui->addSubItem('range-slider', __('Range Slider'), ['url' => '/extended/range-slider']);
            $extended_ui->addSubItem('dragula', __('Dragula'), ['url' => '/extended/dragula']);
            $extended_ui->addSubItem('animation', __('Animation'), ['url' => '/extended/animation']);
            $extended_ui->addSubItem('sweet-alert', __('Sweet Alert'), ['url' => '/extended/sweet-alert']);
            $extended_ui->addSubItem('tour', __('Tour Page'), ['url' => '/extended/tour']);
            $extended_ui->addSubItem('scrollspy', __('Scrollspy'), ['url' => '/extended/scrollspy']);
            $extended_ui->addSubItem('loading-buttons', __('Loading Buttons'), ['url' => '/extended/loading-buttons']);


            $widgets = $menu->addItem('widgets', __('Widgets'),['url'=>'/widgets'])
                ->data('icon', 'gift');



            $icons = $menu->addItem('icons', __('Icons'))
                ->data('icon', 'cpu')
                ->data('arrow', 'menu-arrow');
            $icons->addSubItem('two-tone', __('Two Tone Icons'), ['url' => '/icons/two-tone']);
            $icons->addSubItem('feather', __('Feather Icons'), ['url' => '/icons/feather']);
            $icons->addSubItem('mdi', __('Material Design Icons'), ['url' => '/icons/mdi']);
            $icons->addSubItem('dripicons', __('Dripicons'), ['url' => '/icons/dripicons']);
            $icons->addSubItem('font-awesome', __('Font Awesome 5'), ['url' => '/icons/font-awesome']);
            $icons->addSubItem('themify', __('Themify'), ['url' => '/icons/themify']);
            $icons->addSubItem('simple-line', __('Simple Line'), ['url' => '/icons/simple-line']);
            $icons->addSubItem('weather', __('Weather'), ['url' => '/icons/weather']);



            $forms = $menu->addItem('forms', __('Forms'))
                ->data('icon', 'bookmark')
                ->data('arrow', 'menu-arrow');
            $forms->addSubItem('elements', __('General Elements'), ['url' => '/forms/elements']);
            $forms->addSubItem('advanced', __('Advanced'), ['url' => '/forms/advanced']);
            $forms->addSubItem('validation', __('Validation '), ['url' => '/forms/validation']);
            $forms->addSubItem('pickers', __('Pickers'), ['url' => '/forms/pickers']);
            $forms->addSubItem('wizard', __('Wizard'), ['url' => '/forms/wizard']);
            $forms->addSubItem('masks', __('Masks'), ['url' => '/forms/masks']);
            $forms->addSubItem('summernote', __('Summernote'), ['url' => '/forms/summernote']);
            $forms->addSubItem('quilljs', __('Quilljs Editor'), ['url' => '/forms/quilljs']);
            $forms->addSubItem('file-uploads', __('File Uploads'), ['url' => '/forms/file-uploads']);
            $forms->addSubItem('x-editable', __('X Editable'), ['url' => '/forms/x-editable']);
            $forms->addSubItem('image-crop', __('Image Crop'), ['url' => '/forms/image-crop']);


            $tables = $menu->addItem('tables', __('Tables'))
                ->data('icon', 'grid')
                ->data('arrow', 'menu-arrow');
            $tables->addSubItem('basic', __('Basic Tables'), ['url' => '/tables/basic']);
            $tables->addSubItem('datatables', __('Data Tables'), ['url' => '/tables/datatables']);
            $tables->addSubItem('editable', __('Editable Tables'), ['url' => '/tables/editable']);
            $tables->addSubItem('responsive', __('Responsive Tables'), ['url' => '/tables/responsive']);
            $tables->addSubItem('footables', __('FooTable'), ['url' => '/tables/footables']);
            $tables->addSubItem('bootstrap', __('Bootstrap Tables'), ['url' => '/tables/bootstrap']);
            $tables->addSubItem('tablesaw', __('Tablesaw Tables'), ['url' => '/tables/tablesaw']);
            $tables->addSubItem('jsgrid', __('JsGrid Tables'), ['url' => '/tables/jsgrid']);


            $charts = $menu->addItem('charts', __('Charts'))
                ->data('icon', 'bar-chart-2')
                ->data('arrow', 'menu-arrow');
            $charts->addSubItem('apex', __('Apex Charts'), ['url' => '/charts/apex']);
            $charts->addSubItem('flot', __('Flot Charts'), ['url' => '/charts/flot']);
            $charts->addSubItem('morris', __('Morris Charts'), ['url' => '/charts/morris']);
            $charts->addSubItem('chartjs', __('Chartjs Charts'), ['url' => '/charts/chartjs']);
            $charts->addSubItem('peity', __('Peity Charts'), ['url' => '/charts/peity']);
            $charts->addSubItem('chartist', __('Chartist Charts'), ['url' => '/charts/chartist']);
            $charts->addSubItem('c3', __('C3 Charts'), ['url' => '/charts/c3']);
            $charts->addSubItem('sparklines', __('Sparklines Charts'), ['url' => '/charts/sparklines']);
            $charts->addSubItem('knob', __('Jquery Knob Charts'), ['url' => '/charts/knob']);



            $maps = $menu->addItem('maps', __('Maps'))
                ->data('icon', 'map')
                ->data('arrow', 'menu-arrow');
            $maps->addSubItem('google', __('Google Maps'), ['url' => '/maps/google']);
            $maps->addSubItem('vector', __('Vector Maps'), ['url' => '/maps/vector']);
            $maps->addSubItem('mapael', __('Mapael Maps'), ['url' => '/maps/mapael']);


           /* $multilevel = $menu->addItem('multilevel', __('Multi Level'))
                ->data('icon', 'share-2')
                ->data('arrow', 'menu-arrow');

            $multilevel1 = $multilevel->addSubItem('multilevel1', 'Second Level',[
                'parent' => 'multilevel'
            ])->data('arrow', 'menu-arrow');

            $multilevel1->addSubItem('multilevel2', 'Third Level',[
                'parent' => 'multilevel1'
            ]);*/



        }
    }
}
