<?php

namespace App\Services;

use App\Models\Region;

class RegionService
{

    public function __construct()
    {

    }

    public static function getRegionByName($regionName){
        $region = Region::where('name', $regionName)->first();
        if(empty($region)){
            return null;
        }
        return $region;
    }
}