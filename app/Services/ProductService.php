<?php

namespace App\Services;

use App\Models\Product;
use App\Models\Tender;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Auth;
use App\Models\ProductImage;
use App\Models\Category;
use Illuminate\Support\Facades\DB;

class ProductService
{
    public $distributorCrmId;

    public function __construct($distributorCrmId = false)
    {
        $this->distributorCrmId = $distributorCrmId;
        if (empty($this->distributorCrmId)) {
            throw new \Exception();
        }
    }

    public function uploadPhotos($data, $file): array
    {
        $folder = 'tmp_photo_products/'.\Str::random(20);
        $fullPath = storage_path('app/'.$folder);
        $file->storeAs($folder, '/photos.zip');
        $zip = new \ZipArchive();
        $arrFileName = [];
        $recorded = $loaded = 0;
        $num_folder = 0;
        if ($zip->open($fullPath.'/photos.zip')===true) {
            $fullPath .= '/photos/';
            $loaded = $zip->numFiles;
            //dd($zip->getNameIndex(25, \ZipArchive::FL_ENC_RAW));
            for ($i = 0; $i < $loaded; $i++) {
                $name = $zip->getNameIndex($i, \ZipArchive::FL_ENC_RAW);
                $name = iconv('cp866', 'UTF-8//IGNORE', $name);
                //dd($name);
                if(strpos($name, '.') === false){
                    $num_folder++;
                    continue;
                }
                $arrFileName[$name] = \Str::random(20);
                $zip->renameIndex($i, $arrFileName[$name]);
                $zip->extractTo($fullPath, $arrFileName[$name]);

                $product_id = explode('/',$name);
                $uploadPath = public_path('storage/'.$this->distributorCrmId.'/products/'.$product_id[1]);

                if (!\File::isDirectory($uploadPath)) {
                    \File::makeDirectory($uploadPath, 0755, true);
                }

                $img = Image::make($fullPath.$arrFileName[$name]);

                $mime = substr($name,strrpos($name,'.'));


                if(strpos($name, 'cover') !== false){
                    $fileName = md5($name)."_cover".$mime;
                    $is_cover = true;
                }else{
                    $fileName = md5($name).$mime;
                    $is_cover = false;
                }
                $filePath = $uploadPath.'/'.$fileName;
                if (!\File::isFile($filePath)) {
                    \File::delete($filePath);
                }
                //$data[$key]['cover'] = $fileName.'?v='.time();
                $img->save($filePath, 100, substr($mime,1));
                $recorded++;

                ProductImage::create([
                    'user_id' => $this->distributorCrmId,
                    'product_id' => $product_id[1],
                    'is_cover' => $is_cover,
                    'img_url' => '/storage/'.$this->distributorCrmId.'/products/'.$product_id[1].'/'.$fileName,
                ]);

            }
            $zip->close();
        }
        Storage::deleteDirectory($folder);

        return [
            'data' => $data,
            'message' => 'загружено фото '.$recorded.' из '.($loaded - $num_folder).';',
        ];
    }

    public function processingExcel($file): array
    {

        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        $spreadsheet = $reader->load($file->getRealPath());
        $sheetData = $spreadsheet->getActiveSheet()->toArray();
        $data = [];
        $columns = $this->mappingExcelDb();
        if (!empty($sheetData) && is_array($sheetData)) {
            $columnList = [];
            foreach ($sheetData[0] as $key => $column) {
                $column = mb_strtolower(trim($column));
                foreach ($columns as $label => $code) {
                    $label = mb_strtolower(trim($label));
                    if ($column==$label) {
                        $columnList[$key] = $code;
                    }
                }
            }
            unset($sheetData[0]);

            foreach ($sheetData as $row) {
                $rowData = [];
                foreach ($row as $key => $value) {
                    if (isset($columnList[$key])) {
                        $rowData[$columnList[$key]] = trim($value);
                    }
                }
                $data[] = $rowData;
            }
        }

        return $data;
    }

    public function mappingExcelDb(): array
    {
        return [
            'Наименование товара' => 'name',
            'Баркод' => 'barcode',
            'Бренд' => 'brand',
            'Модель'=>'model',
            'Артикул (sku)' => 'sku',
            'Страна производства' => 'country_made_in',
            'Вес (кг)'=> 'weight',
            'Габариты в упаковке, см (Д/Ш/В)' => 'overall_dimensions',
            'Базовая оптовая цена' => 'base_price',
            'Оптовая цена по дропшиппингу' => 'dropshipping_price',
            'Скидка' => 'discount',
            'Цена по акции' => 'discount_price',
            'РРЦ'  => 'recommended_retail_price',
            'Минимальный заказ (шт)' => 'min_order',
            'Товарная группа' => 'category_group_id',
            'Категория товара' => 'category_id',
            'Характеристики' => 'attributes',
            'Состав' => 'consist',
            'Описание' => 'description',
            'Остаток' => 'stock',
            'Статус' => 'status',
            'Модификация товара(id)' => 'modifications',
            'Единица измерения' => 'measure',
            'Коллекция' => 'collection',
            'Сезон' => 'season',
            'ID' => 'product_id',
            'Сегмент' => 'segment'
        ];
    }

    public static function getCoverImages($products){

        $covers = [];

        for($i = 0; $i < count($products); $i++){
            $covers[$products[$i]['product_id']] = ProductImage::select('product_id', 'img_url')->where('product_id', $products[$i]['product_id'])->where('is_cover', true)->first();
            if($covers[$products[$i]['product_id']] == null)
                unset($covers[$products[$i]['product_id']]);
        }
        return $covers;
    }

    public static function getProducts(){

        if(rand(0,1) || false){
            $user_id = Auth::id();

            $tender_categories = [];

            $tenders = Tender::getTendersByCategoryUserId($user_id);
            foreach ($tenders as $tender){
                foreach ($tender->categories as $category_id){
                    $categories = Category::defaultOrder()->descendantsAndSelf($category_id['category_id']);
                    foreach ($categories as $category => $category_id){
                        $tender_categories[$category_id->id] = true;
                    }
                    //dd($tender_categories);
                }
            }

            $products = Product::whereIn('category_id', array_keys($tender_categories))->where('status', '<>', 'hide')
                ->filters()
                ->defaultSort('id', 'desc')
                ->paginate(30);
        }else{
            $products = Product::where('user_id', Auth::id())
                ->filters()
                ->defaultSort('id', 'desc')
                ->paginate(30);
        }

        $covers = self::getCoverImages($products);
        foreach ($products as $key=>$value){
            foreach ($covers as $k=>$v){
                if($value['product_id'] == $v['product_id']){
                    $products[$key]['cover'] = $covers[$k]['img_url'];
                }
            }
        }

        return $products;
    }

    public static function saveProduct($param)
    {
        $product = Product::create($param);

        return $product;
    }

    public static function updateProduct($param, $product)
    {
        $product = Product::where('id',$product->id)->update($param);

        return $product;
    }

    public static function getProperties($product_id){
        $properties = DB::table('model_property_values')
                        ->join('property_values', 'model_property_values.property_value_id', '=', 'property_values.id')
                        ->join('properties', 'property_values.property_id', '=', 'properties.id')
                        ->select('model_property_values.stock', 'properties.slug', 'property_values.id', 'property_values.value','property_values.title')
                        ->where('model_type','product')
                        ->where('model_id', $product_id)
                        ->get();

        $data = [];

        foreach((array)$properties as $property){
            foreach($property as $key => $value){
                $data[$value->slug][] = $property[$key];
            }
        }

        return $data;
    }

    public static function getTenderProductProperty($sizes = []){

        $products_of_property = [];

        foreach ($sizes as $size){
            $products_size_zero = DB::table('model_property_values')
                ->where('model_type','product')
                ->where('property_value_id',$size)
                ->where('stock','>',0)
                ->get();

            foreach ($products_size_zero as $data){
                if(isset($products_of_property[$data->model_id])){
                    $products_of_property[$data->model_id] += 1;
                }else{
                    $products_of_property[$data->model_id] = 1;
                }
            }

        }

        unset($data);

        foreach ($products_of_property as $k=>$v){
            if(count($sizes) === $v){
                $data[] = $k;
            }
        }

        return $data;
    }

    public static function getUserId(){
        dd(Auth::id());
    }
}
