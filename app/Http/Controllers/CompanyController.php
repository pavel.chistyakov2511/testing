<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Region;
use Illuminate\Http\Request;
use App\Models\Organization;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('lk.companies.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('common');
        /*$categories = Category::get();
        $cities = Region::get();
        return view('lk.companies.create', ['categories'=>$categories, 'cities'=>$cities]);*/
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = [
            'user_id' => Auth::id(),
            'brand' => $request->brand,
            'slug' => Str::slug($request->brand),
            'user_type'=> $request->user_type,
            'min_sum_order' => $request->min_sum_order,
            'segment' => json_encode($request->segment, true),
            'opt' => json_encode($request->opt, true),
            'categories' => json_encode($request->categories,true),
            'description' => $request->description,
            'registration_nr' => $request->company_register_number,
            'email' => $request->email,
            'phone' => $request->phone
        ];

        Organization::create($data);

        if($request->company_type == 'customer')
            return redirect(route('lk.tenders.create'));

        return redirect(route('lk.products.create'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
