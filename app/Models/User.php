<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends \Konekt\AppShell\Models\User implements MustVerifyEmail
{
    use HasApiTokens;

    protected $enums = [
        'type' => UserEnum::class
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];
}
