<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Konekt\Address\Models\Organization as KonektOrganization;
use Laravel\Passport\HasApiTokens;

class Organization extends KonektOrganization
{
    use HasFactory, HasApiTokens;

    protected $table = 'organizations';

    protected $fillable = ['user_id','slug','brand', 'tax_nr', 'registration_nr', 'email', 'phone','user_type','segment','opt', 'categories','description','min_sum_order'];
}
