<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTendersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tenders', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id');
            $table->text('description')->nullable()->default(null);
            $table->jsonb('categories');
            $table->double('sum');
            $table->integer('count')->nullable()->default(null);
            $table->text('volume');
            $table->jsonb('segment');
            $table->integer('region_delivery');
            $table->jsonb('parameters');
            $table->tinyInteger('status')->nullable()->default(null);
            $table->date('date_to')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tenders');
    }
}
