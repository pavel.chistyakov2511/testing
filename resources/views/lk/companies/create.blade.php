@extends('layouts.vertical', ['title' => 'Create Project'])

@section('css')
    <!-- Plugins css -->
    <link href="{{asset('assets/libs/dropzone/dropzone.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/libs/select2/select2.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/libs/flatpickr/flatpickr.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/libs/quill/quill.min.css')}}" rel="stylesheet" type="text/css" />

@endsection

@section('content')
    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">UBold</a></li>
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Projects</a></li>
                            <li class="breadcrumb-item active">Создание компании</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Create Project</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="col-12">
                <form action="{{route('lk.company.store')}}" method="post" id="cart_form">
                    @csrf
                <div class="card">
                    <div class="card-body">

                        <div class="row">
                            <div class="col-xl-6">
                                <div class="form-group">
                                    <label for="branc">Название компании / Бренд</label>
                                    <input type="text" id="brand" class="form-control" name="brand" placeholder="Введите название компании (бренда)">
                                </div>

                                <div class="form-group">
                                    <label for="project-overview">Описание компании</label>
                                    <div id="snow-editor" name="description" style="height: 300px;"></div> <!-- end Snow-editor-->
                                </div>

                                <div class="form-group">
                                    <label>Тип компании</label> <br/>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="manufacturer" name="company_type" class="custom-control-input" value="manufacturer">
                                        <label class="custom-control-label" for="manufacturer">Производитель</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="customer" name="company_type" class="custom-control-input" value="customer">
                                        <label class="custom-control-label" for="customer">Покупатель</label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="project-priority">Ценовой сегмент</label>

                                    <select class="form-control select2-multiple" data-toggle="select2" multiple="multiple" data-placeholder="Выберете ценовую категорию" name="segment[]">
                                        <option value="low">Эконом</option>
                                        <option value="middle">Средний</option>
                                        <option value="high">Высокий</option>
                                        <option value="premium">Премиум</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="project-priority">Опт</label>

                                    <select class="form-control select2-multiple" data-toggle="select2" multiple="multiple" data-placeholder="Выберете оптовые объемы" name="opt[]">
                                        <option value="low">Мелкий</option>
                                        <option value="middle">Средний</option>
                                        <option value="high">Крупный</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="project-budget" class="min_summ_buy" style="display:none">Сумма закупки</label>
                                    <label for="project-budget" class="min_summ_sell">Минимальный заказ</label>
                                    <input type="text" id="project-budget" class="form-control" name="min_sum_order" placeholder="Enter project budget">
                                </div>

                                <div class="form-group">
                                    <label for="project-priority">Категории товаров</label>

                                <select class="form-control select2-multiple" data-toggle="select2" multiple="multiple" name="categories[]">
                                    <option value="0">Выбрать</option>
                                    @foreach($categories as $category)
                                        <option value="{{$category->id}}">{{$category->name}}</option>
                                    @endforeach
                                </select>
                                </div>

                            </div> <!-- end col-->

                            <div class="col-xl-6">
                                <div class="form-group mt-3 mt-xl-0">
                                    <label for="projectname" class="mb-0">Avatar</label>
                                    <p class="text-muted font-14">Recommended thumbnail size 800x400 (px).</p>

                                    <input name="company_img" type="file" />

                                </div>
                                <div class="form-group">
                                    <label for="email" >E-mail:</label>
                                    <input type="email" class="form-control" placeholder="Введите email" name="email">
                                </div>

                                <div class="form-group">
                                    <label for="phone">Телефон:</label>
                                    <input type="phone" class="form-control" placeholder="Введите телефон" name="phone">
                                </div>
                            </div> <!-- end col-->
                        </div>
                        <!-- end row -->


                        <div class="row mt-3">
                            <div class="col-12 text-center">
                                <button type="button" class="btn btn-success waves-effect waves-light m-1" id="create_company"><i class="fe-check-circle mr-1"></i> Create</button>
                                <button type="button" class="btn btn-light waves-effect waves-light m-1"><i class="fe-x mr-1"></i> Cancel</button>
                            </div>
                        </div>

                    </div> <!-- end card-body -->
                </div> <!-- end card-->
                </form>
            </div> <!-- end col-->
        </div>
        <!-- end row-->

    </div> <!-- container -->
@endsection

@section('script')
    <!-- Plugins js-->
    <script src="{{asset('assets/libs/dropzone/dropzone.min.js')}}"></script>
    <script src="{{asset('assets/libs/select2/select2.min.js')}}"></script>
    <script src="{{asset('assets/libs/flatpickr/flatpickr.min.js')}}"></script>

    <!-- Page js-->
    <script src="{{asset('assets/js/pages/create-project.init.js')}}"></script>

    <script src="{{asset('assets/libs/quill/quill.min.js')}}"></script>

    <!-- Page js-->
    <script src="{{asset('assets/js/pages/form-quilljs.init.js')}}"></script>

    <script>
        $('document').ready(function () {
            $('#customer').on('click', function(){
                $('.min_summ_buy').show();
                $('.min_summ_sell').hide();
            });
            $('#manufacturer').on('click', function(){
                $('.min_summ_buy').hide();
                $('.min_summ_sell').show();
            });

            $('button#create_company').click(function () {
                $form = $('#cart_form');
                $form.submit();
            });
        });
    </script>
@endsection
