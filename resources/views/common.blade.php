
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @include('layouts.shared/title-meta', ['title' => ''])
    @include('layouts.shared/head-css')
</head>

<body @yield('body-extra')>

<div id="app">
<!-- Begin page -->
    <app></app>
<!-- END wrapper -->
</div>

@include('layouts.shared/right-sidebar')

@include('layouts.shared/footer-script')
<footer class="footer footer-alt">
    <script>document.write(new Date().getFullYear())</script> &copy; UBold theme by <a href="" class="text-white-50">Coderthemes</a>
</footer>
</body>
</html>
