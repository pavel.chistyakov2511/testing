/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

//Vue.component('example-component', require('./components/ExampleComponent.vue').default);


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

/*const app = new Vue({
    el: '#app',
});*/


import router from "./router";
import axios from 'axios';
import VueAxios from 'vue-axios';
import http from './http/Index';
import store from './store';
import auth from './plugins/auth.js';
import JQuery from 'jquery';
import JData from './layout';

//import AppJS from './plugins/App';


axios.defaults.baseURL = 'http://site.test/api';

const Data = {
    router,
    JQuery,
    JData
};

const app = Vue.createApp(Data);

//app.component('example-component', require('./components/ExampleComponent').default);

app.component('app', require('./components/App').default);

app.component('spinner', require('./components/SpinnerComponent').default);
app.component('topbar', require('./components/TopbarComponent').default);
app.component('left-sidebar', require('./components/LeftSideBarComponent').default);

app.use(router);
app.use(http);
app.use(store);
app.use(auth);
app.use(VueAxios, axios);
//app.use(AppJS);

app.mount('#app');
