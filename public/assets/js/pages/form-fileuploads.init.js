/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 25);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/pages/form-fileuploads.init.js":
/*!*****************************************************!*\
  !*** ./resources/js/pages/form-fileuploads.init.js ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("/*\r\nTemplate Name: Ubold - Responsive Bootstrap 4 Admin Dashboard\r\nAuthor: CoderThemes\r\nWebsite: https://coderthemes.com/\r\nContact: support@coderthemes.com\r\nFile: File uploads init js\r\n*/\n// Dropzone\n!function ($) {\n  \"use strict\";\n\n  var FileUpload = function FileUpload() {\n    this.$body = $(\"body\");\n  };\n  /* Initializing */\n\n\n  FileUpload.prototype.init = function () {\n    // Disable auto discovery\n    Dropzone.autoDiscover = false;\n    $('[data-plugin=\"dropzone\"]').each(function () {\n      var actionUrl = $(this).attr('action');\n      var previewContainer = $(this).data('previewsContainer');\n      var opts = {\n        url: actionUrl,\n        method: 'post',\n        uploadMultiple: true,\n        paramName: 'images',\n        forceFallback: false,\n        hiddenInputContainer: \"form.image_upload\",\n        maxFiles: 10,\n        addRemoveLinks: false,\n        clickable: false,\n        headers: {\n          'X-CSRF-TOKEN': $('meta[name=\"csrf-token\"]').attr('content')\n        }\n      };\n\n      if (previewContainer) {\n        opts['previewsContainer'] = previewContainer;\n      }\n\n      var uploadPreviewTemplate = $(this).data(\"uploadPreviewTemplate\");\n\n      if (uploadPreviewTemplate) {\n        opts['previewTemplate'] = $(uploadPreviewTemplate).html();\n      }\n\n      var dropzoneEl = $(this).dropzone(opts);\n    });\n  }, //init fileupload\n  $.FileUpload = new FileUpload(), $.FileUpload.Constructor = FileUpload;\n}(window.jQuery), //initializing FileUpload\nfunction ($) {\n  \"use strict\";\n\n  $.FileUpload.init();\n}(window.jQuery);\n\nif ($('[data-plugins=\"dropify\"]').length > 0) {\n  // Dropify\n  $('[data-plugins=\"dropify\"]').dropify({\n    messages: {\n      'default': 'Drag and drop a file here or click',\n      'replace': 'Drag and drop or click to replace',\n      'remove': 'Remove',\n      'error': 'Ooops, something wrong appended.'\n    },\n    error: {\n      'fileSize': 'The file size is too big (1M max).'\n    }\n  });\n}//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvanMvcGFnZXMvZm9ybS1maWxldXBsb2Fkcy5pbml0LmpzP2M4MGEiXSwibmFtZXMiOlsiJCIsIkZpbGVVcGxvYWQiLCIkYm9keSIsInByb3RvdHlwZSIsImluaXQiLCJEcm9wem9uZSIsImF1dG9EaXNjb3ZlciIsImVhY2giLCJhY3Rpb25VcmwiLCJhdHRyIiwicHJldmlld0NvbnRhaW5lciIsImRhdGEiLCJvcHRzIiwidXJsIiwibWV0aG9kIiwidXBsb2FkTXVsdGlwbGUiLCJwYXJhbU5hbWUiLCJmb3JjZUZhbGxiYWNrIiwiaGlkZGVuSW5wdXRDb250YWluZXIiLCJtYXhGaWxlcyIsImFkZFJlbW92ZUxpbmtzIiwiY2xpY2thYmxlIiwiaGVhZGVycyIsInVwbG9hZFByZXZpZXdUZW1wbGF0ZSIsImh0bWwiLCJkcm9wem9uZUVsIiwiZHJvcHpvbmUiLCJDb25zdHJ1Y3RvciIsIndpbmRvdyIsImpRdWVyeSIsImxlbmd0aCIsImRyb3BpZnkiLCJtZXNzYWdlcyIsImVycm9yIl0sIm1hcHBpbmdzIjoiQUFBQTs7Ozs7OztBQVNBO0FBQ0EsQ0FBQyxVQUFVQSxDQUFWLEVBQWE7QUFDVjs7QUFFQSxNQUFJQyxVQUFVLEdBQUcsU0FBYkEsVUFBYSxHQUFZO0FBQ3pCLFNBQUtDLEtBQUwsR0FBYUYsQ0FBQyxDQUFDLE1BQUQsQ0FBZDtBQUNILEdBRkQ7QUFLQTs7O0FBQ0FDLFlBQVUsQ0FBQ0UsU0FBWCxDQUFxQkMsSUFBckIsR0FBNEIsWUFBWTtBQUNwQztBQUVBQyxZQUFRLENBQUNDLFlBQVQsR0FBd0IsS0FBeEI7QUFFQU4sS0FBQyxDQUFDLDBCQUFELENBQUQsQ0FBOEJPLElBQTlCLENBQW1DLFlBQVk7QUFDM0MsVUFBSUMsU0FBUyxHQUFHUixDQUFDLENBQUMsSUFBRCxDQUFELENBQVFTLElBQVIsQ0FBYSxRQUFiLENBQWhCO0FBQ0EsVUFBSUMsZ0JBQWdCLEdBQUdWLENBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUVcsSUFBUixDQUFhLG1CQUFiLENBQXZCO0FBRUEsVUFBSUMsSUFBSSxHQUFHO0FBQ1BDLFdBQUcsRUFBRUwsU0FERTtBQUVQTSxjQUFNLEVBQUUsTUFGRDtBQUdQQyxzQkFBYyxFQUFFLElBSFQ7QUFJUEMsaUJBQVMsRUFBRSxRQUpKO0FBS1BDLHFCQUFhLEVBQUUsS0FMUjtBQU1QQyw0QkFBb0IsRUFBRSxtQkFOZjtBQU9QQyxnQkFBUSxFQUFFLEVBUEg7QUFRUEMsc0JBQWMsRUFBRSxLQVJUO0FBU1BDLGlCQUFTLEVBQUUsS0FUSjtBQVVQQyxlQUFPLEVBQUU7QUFDTCwwQkFBZ0J0QixDQUFDLENBQUMseUJBQUQsQ0FBRCxDQUE2QlMsSUFBN0IsQ0FBa0MsU0FBbEM7QUFEWDtBQVZGLE9BQVg7O0FBY0EsVUFBSUMsZ0JBQUosRUFBc0I7QUFDbEJFLFlBQUksQ0FBQyxtQkFBRCxDQUFKLEdBQTRCRixnQkFBNUI7QUFDSDs7QUFFRCxVQUFJYSxxQkFBcUIsR0FBR3ZCLENBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUVcsSUFBUixDQUFhLHVCQUFiLENBQTVCOztBQUNBLFVBQUlZLHFCQUFKLEVBQTJCO0FBQ3ZCWCxZQUFJLENBQUMsaUJBQUQsQ0FBSixHQUEwQlosQ0FBQyxDQUFDdUIscUJBQUQsQ0FBRCxDQUF5QkMsSUFBekIsRUFBMUI7QUFDSDs7QUFFRCxVQUFJQyxVQUFVLEdBQUd6QixDQUFDLENBQUMsSUFBRCxDQUFELENBQVEwQixRQUFSLENBQWlCZCxJQUFqQixDQUFqQjtBQUVILEtBN0JEO0FBOEJILEdBbkNELEVBcUNJO0FBQ0FaLEdBQUMsQ0FBQ0MsVUFBRixHQUFlLElBQUlBLFVBQUosRUF0Q25CLEVBc0NtQ0QsQ0FBQyxDQUFDQyxVQUFGLENBQWEwQixXQUFiLEdBQTJCMUIsVUF0QzlEO0FBd0NILENBakRBLENBaURDMkIsTUFBTSxDQUFDQyxNQWpEUixDQUFELEVBbURBO0FBQ0EsVUFBVTdCLENBQVYsRUFBYTtBQUNiOztBQUNJQSxHQUFDLENBQUNDLFVBQUYsQ0FBYUcsSUFBYjtBQUNILENBSEQsQ0FHRXdCLE1BQU0sQ0FBQ0MsTUFIVCxDQXBEQTs7QUEwREEsSUFBSTdCLENBQUMsQ0FBQywwQkFBRCxDQUFELENBQThCOEIsTUFBOUIsR0FBdUMsQ0FBM0MsRUFBOEM7QUFDMUM7QUFDQTlCLEdBQUMsQ0FBQywwQkFBRCxDQUFELENBQThCK0IsT0FBOUIsQ0FBc0M7QUFDbENDLFlBQVEsRUFBRTtBQUNOLGlCQUFXLG9DQURMO0FBRU4saUJBQVcsbUNBRkw7QUFHTixnQkFBVSxRQUhKO0FBSU4sZUFBUztBQUpILEtBRHdCO0FBT2xDQyxTQUFLLEVBQUU7QUFDSCxrQkFBWTtBQURUO0FBUDJCLEdBQXRDO0FBV0giLCJmaWxlIjoiLi9yZXNvdXJjZXMvanMvcGFnZXMvZm9ybS1maWxldXBsb2Fkcy5pbml0LmpzLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxuVGVtcGxhdGUgTmFtZTogVWJvbGQgLSBSZXNwb25zaXZlIEJvb3RzdHJhcCA0IEFkbWluIERhc2hib2FyZFxyXG5BdXRob3I6IENvZGVyVGhlbWVzXHJcbldlYnNpdGU6IGh0dHBzOi8vY29kZXJ0aGVtZXMuY29tL1xyXG5Db250YWN0OiBzdXBwb3J0QGNvZGVydGhlbWVzLmNvbVxyXG5GaWxlOiBGaWxlIHVwbG9hZHMgaW5pdCBqc1xyXG4qL1xyXG5cclxuXHJcbi8vIERyb3B6b25lXHJcbiFmdW5jdGlvbiAoJCkge1xyXG4gICAgXCJ1c2Ugc3RyaWN0XCI7XHJcblxyXG4gICAgdmFyIEZpbGVVcGxvYWQgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgdGhpcy4kYm9keSA9ICQoXCJib2R5XCIpXHJcbiAgICB9O1xyXG5cclxuXHJcbiAgICAvKiBJbml0aWFsaXppbmcgKi9cclxuICAgIEZpbGVVcGxvYWQucHJvdG90eXBlLmluaXQgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgLy8gRGlzYWJsZSBhdXRvIGRpc2NvdmVyeVxyXG5cclxuICAgICAgICBEcm9wem9uZS5hdXRvRGlzY292ZXIgPSBmYWxzZTtcclxuXHJcbiAgICAgICAgJCgnW2RhdGEtcGx1Z2luPVwiZHJvcHpvbmVcIl0nKS5lYWNoKGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgdmFyIGFjdGlvblVybCA9ICQodGhpcykuYXR0cignYWN0aW9uJylcclxuICAgICAgICAgICAgdmFyIHByZXZpZXdDb250YWluZXIgPSAkKHRoaXMpLmRhdGEoJ3ByZXZpZXdzQ29udGFpbmVyJyk7XHJcblxyXG4gICAgICAgICAgICB2YXIgb3B0cyA9IHsgXHJcbiAgICAgICAgICAgICAgICB1cmw6IGFjdGlvblVybCwgXHJcbiAgICAgICAgICAgICAgICBtZXRob2Q6ICdwb3N0JyxcclxuICAgICAgICAgICAgICAgIHVwbG9hZE11bHRpcGxlOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgcGFyYW1OYW1lOiAnaW1hZ2VzJyxcclxuICAgICAgICAgICAgICAgIGZvcmNlRmFsbGJhY2s6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgaGlkZGVuSW5wdXRDb250YWluZXI6IFwiZm9ybS5pbWFnZV91cGxvYWRcIixcclxuICAgICAgICAgICAgICAgIG1heEZpbGVzOiAxMCxcclxuICAgICAgICAgICAgICAgIGFkZFJlbW92ZUxpbmtzOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgIGNsaWNrYWJsZTogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICBoZWFkZXJzOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgJ1gtQ1NSRi1UT0tFTic6ICQoJ21ldGFbbmFtZT1cImNzcmYtdG9rZW5cIl0nKS5hdHRyKCdjb250ZW50JylcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgaWYgKHByZXZpZXdDb250YWluZXIpIHtcclxuICAgICAgICAgICAgICAgIG9wdHNbJ3ByZXZpZXdzQ29udGFpbmVyJ10gPSBwcmV2aWV3Q29udGFpbmVyO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICB2YXIgdXBsb2FkUHJldmlld1RlbXBsYXRlID0gJCh0aGlzKS5kYXRhKFwidXBsb2FkUHJldmlld1RlbXBsYXRlXCIpO1xyXG4gICAgICAgICAgICBpZiAodXBsb2FkUHJldmlld1RlbXBsYXRlKSB7XHJcbiAgICAgICAgICAgICAgICBvcHRzWydwcmV2aWV3VGVtcGxhdGUnXSA9ICQodXBsb2FkUHJldmlld1RlbXBsYXRlKS5odG1sKCk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIHZhciBkcm9wem9uZUVsID0gJCh0aGlzKS5kcm9wem9uZShvcHRzKTtcclxuICAgICAgICAgICAgXHJcbiAgICAgICAgfSk7XHJcbiAgICB9LFxyXG5cclxuICAgICAgICAvL2luaXQgZmlsZXVwbG9hZFxyXG4gICAgICAgICQuRmlsZVVwbG9hZCA9IG5ldyBGaWxlVXBsb2FkLCAkLkZpbGVVcGxvYWQuQ29uc3RydWN0b3IgPSBGaWxlVXBsb2FkXHJcblxyXG59KHdpbmRvdy5qUXVlcnkpLFxyXG5cclxuLy9pbml0aWFsaXppbmcgRmlsZVVwbG9hZFxyXG5mdW5jdGlvbiAoJCkge1xyXG5cInVzZSBzdHJpY3RcIjtcclxuICAgICQuRmlsZVVwbG9hZC5pbml0KClcclxufSh3aW5kb3cualF1ZXJ5KTtcclxuXHJcblxyXG5pZiAoJCgnW2RhdGEtcGx1Z2lucz1cImRyb3BpZnlcIl0nKS5sZW5ndGggPiAwKSB7XHJcbiAgICAvLyBEcm9waWZ5XHJcbiAgICAkKCdbZGF0YS1wbHVnaW5zPVwiZHJvcGlmeVwiXScpLmRyb3BpZnkoe1xyXG4gICAgICAgIG1lc3NhZ2VzOiB7XHJcbiAgICAgICAgICAgICdkZWZhdWx0JzogJ0RyYWcgYW5kIGRyb3AgYSBmaWxlIGhlcmUgb3IgY2xpY2snLFxyXG4gICAgICAgICAgICAncmVwbGFjZSc6ICdEcmFnIGFuZCBkcm9wIG9yIGNsaWNrIHRvIHJlcGxhY2UnLFxyXG4gICAgICAgICAgICAncmVtb3ZlJzogJ1JlbW92ZScsXHJcbiAgICAgICAgICAgICdlcnJvcic6ICdPb29wcywgc29tZXRoaW5nIHdyb25nIGFwcGVuZGVkLidcclxuICAgICAgICB9LFxyXG4gICAgICAgIGVycm9yOiB7XHJcbiAgICAgICAgICAgICdmaWxlU2l6ZSc6ICdUaGUgZmlsZSBzaXplIGlzIHRvbyBiaWcgKDFNIG1heCkuJ1xyXG4gICAgICAgIH1cclxuICAgIH0pO1xyXG59Il0sInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./resources/js/pages/form-fileuploads.init.js\n");

/***/ }),

/***/ 25:
/*!***********************************************************!*\
  !*** multi ./resources/js/pages/form-fileuploads.init.js ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! W:\domains\site.test\resources\js\pages\form-fileuploads.init.js */"./resources/js/pages/form-fileuploads.init.js");


/***/ })

/******/ });