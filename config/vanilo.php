<?php

return [
    // ...
    'framework' => [
        'image'       => [
            'product' => [
                'variants' => [
                    'thumbnail' => [ // Name of the image variant
                        'width'  => 250,
                        'height' => 250,
                        'fit'    => 'crop'
                    ],
                    'cart' => [ // Image variant names can be arbitrary
                        'width'  => 120,
                        'height' => 90,
                        'fit'    => 'crop'
                    ]
                ]
            ],
            'taxon' => [
                'variants' => [
                    'thumbnail' => [
                        'width'  => 320,
                        'height' => 180,
                        'fit'    => 'crop'
                    ],
                    'banner' => [
                        'width'  => 1248,
                        'height' => 702,
                        'fit'    => 'crop'
                    ]
                ]
            ]
        ]
    ]
    //...
];