<?php

return [
    'modules' => [
        Konekt\AppShell\Providers\ModuleServiceProvider::class => [
            'ui' => [
                'name' => 'Vanilo',
                'url' => '/admin/product'
            ],
            'menu'            => [
                'left_nav_menu' => [
                    'share'          => 'leftNavMenu',
                    'cascade_data'   => false,
                    'active_element' => 'link'
                ]
            ],
            'views' => [
                'namespace' => 'appshell'
            ],
            'migrations' => false,
        ],
        Vanilo\Framework\Providers\ModuleServiceProvider::class => [
            'migrations' => false,
        ],
    ],
    'register_route_models' => true,
];
